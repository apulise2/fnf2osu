import json 
from pydub import AudioSegment
import os

precentDiff = 100.0338972002216355399106939148/100 # pydub seems to add somesort of offset, this is suppost to compensate for that.
def speed_change(sound, speed=1.0):
    # Manually override the frame_rate. This tells the computer how many
    # samples to play per second
    sound_with_altered_frame_rate = sound._spawn(sound.raw_data, overrides={
         "frame_rate": int(sound.frame_rate * speed)
      })
     # convert the sound with altered frame rate to a standard frame rate
     # so that regular playback programs will work right. They often only
     # know how to play audio at standard frame rate (like 44.1k)
    return sound_with_altered_frame_rate.set_frame_rate(sound.frame_rate)


def ConvertToOsu(fnfFileName, audio1, audio2):
    outputName = fnfFileName
    jsonToReadFrom = fnfFileName
    header = """osu file format v14

[General]
AudioFilename: audio.mp3
AudioLeadIn: 0
PreviewTime: -1
Countdown: 0
SampleSet: Normal
StackLeniency: 0.7
Mode: 3
LetterboxInBreaks: 0
SpecialStyle: 0
WidescreenStoryboard: 1

[Editor]
DistanceSpacing: 1
BeatDivisor: 4
GridSize: 4
TimelineZoom: 1

[Metadata]
Title:{0}
TitleUnicode:{0}
Artist:fnf
ArtistUnicode:fnf
Creator:fnf
Version:asdasd
Source:
Tags:
BeatmapID:0
BeatmapSetID:-1

[Difficulty]
HPDrainRate:5
CircleSize:4
OverallDifficulty:5
ApproachRate:5
SliderMultiplier:1.4
SliderTickRate:1

[Events]
//Background and Video events
//Break Periods
//Storyboard Layer 0 (Background)
//Storyboard Layer 1 (Fail)
//Storyboard Layer 2 (Pass)
//Storyboard Layer 3 (Foreground)
//Storyboard Layer 4 (Overlay)
//Storyboard Sound Samples

[TimingPoints]
0,500,4,1,0,100,1,0


[HitObjects]
""".format(jsonToReadFrom)
    try:
        os.mkdir(outputName)
    except OSError:
        print ("Creation of the directory %s failed" % outputName)
    else:
        print ("Successfully created the directory %s " % outputName)
    
    #ftransc -f ogg audio1+".ogg"
    audio1 = "F:\map parser\\"+audio1+".ogg"
    audio2 = "F:\map parser\\"+audio2+".ogg"
    sound1 = AudioSegment.from_file(audio1,format="ogg")
    sound2 = AudioSegment.from_file(audio2,format="ogg")
    combined = sound1.overlay(sound2)
    combined = speed_change(combined,precentDiff) #adjusting for weired bug in the convertion
    combined.export(outputName+"\\" +"audio.mp3" , format='mp3')
    
    
    
    
    

    

    f = open(jsonToReadFrom+'.json',) 
    output = open (outputName+"\\"+jsonToReadFrom+" - "+jsonToReadFrom+' (fnf) ['+jsonToReadFrom+'].osu', "w") 
    output.write(header)
    data = json.load(f) 
    print("reading file\n")
    for i in data['song']['notes']: 
        if i['mustHitSection'] == True:

            for each in i['sectionNotes']:
                line = "";

                if each[1] == 0:
                    x= 64
                if each[1] == 1:
                    x= 192
                if each[1] == 2:
                    x= 320
                if each[1] == 3:
                    x= 448

                time = each[0] 
                hitSound = 0
                endTime =  each[2] 
                NoteType = 1
                if endTime != 0 :
                    NoteType = 128
                    endTime += time
                    endTime = int(endTime)
                hitSample = ":0:0:0:0:"
                osuLine = "{0},{1},{2},{3},{4},{5}{6}".format(x,192,time,NoteType,0,endTime,hitSample)
                output.write(osuLine+ "\n")
                for noteNumber in range(4):    
                    if each[1] == noteNumber:
                        line+=("[x]")
                    else:
                        line+=("[ ]")
                print(line + " - " +osuLine ) 

    f.close() 
    output.close()

if __name__ == "__main__":
    ConvertToOsu("ballistic-hard","Inst","Voices")